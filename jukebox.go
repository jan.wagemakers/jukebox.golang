package main

import (
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

type Data struct {
	Prev, Now, Next, Back, Forward, Stop, Start string
}

var command = ""
var data Data

func handler(w http.ResponseWriter, r *http.Request) {
	button := r.FormValue("action")

	// command == Play/Start --> music is playing
	if command == "Play" || command == "Start" {
		if button == "Stop" || button == "Forward" || button == "Back" {
			command = button
		}
	}

	// command == "" --> music is not playing
	if command == "" {
		if button == "Start" {
			command = button
		}
	}

	// set color of buttons on webpage
	data.Stop, data.Start, data.Back, data.Forward = "stop", "off", "off", "off"
	if command == "Play" || command == "Start" {
		data.Stop, data.Start = "off", "on"
	}
	if command == "Forward" {
		data.Forward, data.Stop = "on", "off"
	}
	if command == "Back" {
		data.Back, data.Stop = "on", "off"
	}

	template, err := template.ParseFiles("web/index.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = template.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func background(player, dir string) {
	cmd := exec.Command("")

	now := 0
	var ls []string

	for {

		time.Sleep(500 * time.Millisecond)

		if now < 0 {
			now = 0
		}
		if now >= len(ls) {
			now = 0
			// Get all the songs in jukebox
			err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
				if !info.IsDir() {
					ls = append(ls, path)
				}
				return nil
			})
			if err != nil {
				panic(err)
			}
			// randomize the playlist
			rand.Seed(time.Now().Unix())
			rand.Shuffle(len(ls), func(a, b int) {
				ls[a], ls[b] = ls[b], ls[a]
			})
		}
		if now == 0 {
			data.Prev = "-"
		} else {
			data.Prev = filepath.Base(ls[now-1])
		}
		data.Now = filepath.Base(ls[now])
		if now > len(ls)-2 {
			data.Next = "-"
		} else {
			data.Next = filepath.Base(ls[now+1])
		}

		if command == "Play" {
			if cmd.ProcessState != nil {
				// player has stopped -> play the next song
				now++
				command = "Start"
			}
		}
		if command == "Start" {
			fmt.Println("play: " + ls[now])
			cmd = exec.Command(player, ls[now])
			cmd.Start()
			// ProcessState contains information about an exited process,
			// available after a call to Wait or Run.
			go cmd.Wait()
			command = "Play"
		}
		if command == "Stop" {
			cmd.Process.Kill()
			command = ""
		}
		if command == "Forward" {
			cmd.Process.Kill()
			now++
			command = "Start"
		}
		if command == "Back" {
			cmd.Process.Kill()
			now--
			command = "Start"
		}
	}
}

func main() {
	if len(os.Args) != 3 {
		fmt.Printf("usage: %s [player] [dir]\n", os.Args[0])
		os.Exit(2)
	}

	go background(os.Args[1], os.Args[2])

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("web"))))

	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
